using YourWay.Entity;

namespace YourWay.Repository.Interface
{
    public interface IEventSportRepository: ICrudRepository<EventSport>
    {
         
    }
}