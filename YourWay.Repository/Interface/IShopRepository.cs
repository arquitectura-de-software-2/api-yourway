using YourWay.Entity;

namespace YourWay.Repository.Interface
{
    public interface IShopRepository: ICrudRepository<Shop>
    {
         
    }
}