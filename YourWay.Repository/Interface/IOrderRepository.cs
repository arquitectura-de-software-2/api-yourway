using YourWay.Entity;

namespace YourWay.Repository.Interface
{
    public interface IOrderRepository: ICrudRepository<Order>
    {
         
    }
}