using YourWay.Entity;

namespace YourWay.Repository.Interface
{
    public interface ICategoryRepository: ICrudRepository<Category>
    {
         
    }
}