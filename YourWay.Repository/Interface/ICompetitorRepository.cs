using YourWay.Entity;

namespace YourWay.Repository.Interface
{
    public interface ICompetitorRepository: ICrudRepository<Competitor>
    {
         
    }
}