using System.Collections.Generic;
using System.Linq;
using YourWay.Entity;
using YourWay.Repository.Context;
using YourWay.Repository.Interface;

namespace YourWay.Repository.Implementation
{
    public class EventSportRepository : IEventSportRepository
    {
        private ApplicationDbContext context;
        public EventSportRepository(ApplicationDbContext context){
            this.context = context;
        }

        
        public bool Create(EventSport entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<EventSport> FindAll()
        {
            throw new System.NotImplementedException();
        }

        public EventSport FindById(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Update(EventSport entity)
        {
            throw new System.NotImplementedException();
        }
    }
}