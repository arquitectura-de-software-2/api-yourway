using System.Collections.Generic;
using System.Linq;
using YourWay.Entity;
using YourWay.Repository.Context;
using YourWay.Repository.Interface;

namespace YourWay.Repository.Implementation
{
    public class ShopRepository : IShopRepository
    {
        private ApplicationDbContext context;
        public ShopRepository(ApplicationDbContext context){
            this.context = context;
        }


        public bool Create(Shop entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Shop> FindAll()
        {
            var resultado = new List<Shop>();
            
            try{
                resultado = context.Shop.ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public Shop FindById(int id)
        {
            var resultado = new Shop();

            try{
                resultado = context.Shop.Single(p => p.Id == id);
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public bool Update(Shop entity)
        {
            throw new System.NotImplementedException();
        }
    }
}