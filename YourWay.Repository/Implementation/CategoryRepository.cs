using System.Collections.Generic;
using System.Linq;
using YourWay.Entity;
using YourWay.Repository.Context;
using YourWay.Repository.Interface;

namespace YourWay.Repository.Implementation
{
    public class CategoryRepository : ICategoryRepository
    {
        private ApplicationDbContext context;
        public CategoryRepository(ApplicationDbContext context){
            this.context = context;
        }


        public bool Create(Category entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Category> FindAll()
        {
            var resultado = new List<Category>();
            
            try{
                resultado = context.Category.ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public Category FindById(int id)
        {
            var resultado = new Category();

            try{
                resultado = context.Category.Single(p => p.Id == id);
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public bool Update(Category entity)
        {
            throw new System.NotImplementedException();
        }
    }
}