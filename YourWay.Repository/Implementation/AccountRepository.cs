using System.Collections.Generic;
using System.Linq;
using YourWay.Entity;
using YourWay.Repository.Context;
using YourWay.Repository.Interface;

namespace YourWay.Repository.Implementation
{
    public class AccountRepository : IAccountRepository
    {
        private ApplicationDbContext context;
        public AccountRepository(ApplicationDbContext context){
            this.context = context;
        }


        public bool Create(Account entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Account> FindAll()
        {
            var resultado = new List<Account>();
            
            try{
                resultado = context.Account.ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public Account FindById(int id)
        {
            var resultado = new Account();

            try{
                resultado = context.Account.Single(p => p.Id == id);
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public bool Update(Account entity)
        {
            throw new System.NotImplementedException();
        }
    }
}