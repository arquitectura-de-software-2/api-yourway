using System.Collections.Generic;
using System.Linq;
using YourWay.Entity;
using YourWay.Repository.Context;
using YourWay.Repository.Interface;

namespace YourWay.Repository.Implementation
{
    public class SportsmanRepository : ISportsmanRepository
    {
        private ApplicationDbContext context;
        public SportsmanRepository(ApplicationDbContext context){
            this.context = context;
        }


        public bool Create(Sportsman entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Sportsman> FindAll()
        {
            var resultado = new List<Sportsman>();
            
            try{
                resultado = context.Sportsman.ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;

        }

        public Sportsman FindById(int id)
        {
            var resultado = new Sportsman();

            try{
                resultado = context.Sportsman.Single(p => p.Id == id);
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public bool Update(Sportsman entity)
        {
            try{
                var SportsmanOriginal = context.Sportsman.Single(p => p.Id == entity.Id);

                SportsmanOriginal.Id = entity.Id;
                SportsmanOriginal.FirstName = entity.FirstName;
                SportsmanOriginal.LastName = entity.LastName;
                SportsmanOriginal.BirthDay = entity.BirthDay;

                context.Update(SportsmanOriginal);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }
    }
}