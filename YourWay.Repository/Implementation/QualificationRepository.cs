using System.Collections.Generic;
using System.Linq;
using YourWay.Entity;
using YourWay.Repository.Context;
using YourWay.Repository.Interface;

namespace YourWay.Repository.Implementation
{
    public class QualificationRepository : IQualificationRepository
    {
        private ApplicationDbContext context;
        public QualificationRepository(ApplicationDbContext context){
            this.context = context;
        }


        public bool Create(Qualification entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Qualification> FindAll()
        {
            var resultado = new List<Qualification>();
            
            try{
                resultado = context.Qualification.ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public Qualification FindById(int id)
        {
            var resultado = new Qualification();

            try{
                resultado = context.Qualification.Single(p => p.Id == id);
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public bool Update(Qualification entity)
        {
            try{
                var QualificationOriginal = context.Qualification.Single(p => p.Id == entity.Id);

                QualificationOriginal.Id = entity.Id;
                QualificationOriginal.Comment = entity.Comment;
                QualificationOriginal.Score = entity.Score;

                context.Update(QualificationOriginal);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }
    }
}