﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace YourWay.Repository.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Place",
                table: "Location",
                nullable: true,
                oldClrType: typeof(double));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "Place",
                table: "Location",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
