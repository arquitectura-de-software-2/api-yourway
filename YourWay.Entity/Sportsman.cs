using System;

namespace YourWay.Entity
{
    public class Sportsman
    {
        public int Id {get; set;}
        public string FirstName {get; set;}
        public string LastName {get; set;}
        public DateTime BirthDay {get; set;}

    }
}