namespace YourWay.Entity
{
    public class Friend
    {
        public int Id {get; set;}

        public int SportsManID1 {get; set;}
        public Sportsman SportsMan1 {get; set;}
        public int SportsManID2 {get; set;}
        public Sportsman SportsMan2 {get; set;}
    }
}