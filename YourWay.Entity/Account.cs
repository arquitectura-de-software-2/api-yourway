namespace YourWay.Entity
{
    public class Account
    {
        public int Id {get; set;}
        public string Mail {get; set;}
        public string Password {get; set;}

        public int SportsManId {get; set;}
        public Sportsman SportsMan {get; set;}
    }
}