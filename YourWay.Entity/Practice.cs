namespace YourWay.Entity
{
    public class Practice
    {
        public int Id {get; set;}

        public int SportsManId {get; set;}
        public Sportsman SportsMan {get; set;}
        public int SportId {get; set;}
        public Sport Sport {get; set;}

    }
}