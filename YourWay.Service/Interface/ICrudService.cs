using System.Collections.Generic;

namespace YourWay.Service.Interface
{
    public interface ICrudService<T>
    {
        bool Create(T entity);
        bool Update(T entity);
        bool Delete(int id);
        IEnumerable<T> FindAll();
        T FindById(int id);
    }
}