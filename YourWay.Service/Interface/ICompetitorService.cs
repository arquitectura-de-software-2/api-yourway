using YourWay.Entity;

namespace YourWay.Service.Interface
{
    public interface ICompetitorService: ICrudService<Competitor>
    {
         
    }
}