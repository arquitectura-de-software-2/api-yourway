using System.Collections.Generic;
using YourWay.Entity;
using YourWay.Repository.Interface;
using YourWay.Service.Interface;

namespace YourWay.Service.Implementation
{
    public class EventService : IEventService
    {
        private IEventRepository eventRepository;
        public EventService(IEventRepository eventRepository){
            this.eventRepository = eventRepository;
        }


        public bool Create(Event entity)
        {
            return eventRepository.Create(entity);
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Event> FindAll()
        {
            return eventRepository.FindAll();
        }

        public Event FindById(int id)
        {
            return eventRepository.FindById(id);
        }

        public bool Update(Event entity)
        {
            return eventRepository.Update(entity);
        }
    }
}