using System.Collections.Generic;
using YourWay.Entity;
using YourWay.Repository.Interface;
using YourWay.Service.Interface;

namespace YourWay.Service.Implementation
{
    public class LocationService : ILocationService
    {
        private ILocationRepository locationRepository;
        public LocationService(ILocationRepository locationRepository){
            this.locationRepository = locationRepository;
        }


        public bool Create(Location entity)
        {
            return locationRepository.Create(entity);
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Location> FindAll()
        {
            return locationRepository.FindAll();
        }

        public Location FindById(int id)
        {
            return locationRepository.FindById(id);
        }

        public bool Update(Location entity)
        {
            throw new System.NotImplementedException();
        }
    }
}