using System.Collections.Generic;
using YourWay.Entity;
using YourWay.Repository.Interface;
using YourWay.Service.Interface;

namespace YourWay.Service.Implementation
{
    public class OrderProductService : IOrderProductService
    {
        private IOrderProductRepository orderProductRepository;
        public OrderProductService(IOrderProductRepository orderProductRepository){
            this.orderProductRepository = orderProductRepository;
        }


        public bool Create(OrderProduct entity)
        {
            return orderProductRepository.Create(entity);
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<OrderProduct> FindAll()
        {
            return orderProductRepository.FindAll();
        }

        public OrderProduct FindById(int id)
        {
            return orderProductRepository.FindById(id);
        }

        public bool Update(OrderProduct entity)
        {
            throw new System.NotImplementedException();
        }
    }
}