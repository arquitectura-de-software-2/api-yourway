using System.Collections.Generic;
using YourWay.Entity;
using YourWay.Repository.Interface;
using YourWay.Service.Interface;

namespace YourWay.Service.Implementation
{
    public class FriendService : IFriendService
    {
        private IFriendRepository friendRepository;
        public FriendService(IFriendRepository friendRepository){
            this.friendRepository = friendRepository;
        }


        public bool Create(Friend entity)
        {
            return friendRepository.Create(entity);
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Friend> FindAll()
        {
            throw new System.NotImplementedException();
        }

        public Friend FindById(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Update(Friend entity)
        {
            throw new System.NotImplementedException();
        }
    }
}