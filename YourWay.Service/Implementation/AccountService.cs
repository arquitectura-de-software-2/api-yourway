using System.Collections.Generic;
using YourWay.Entity;
using YourWay.Repository.Interface;
using YourWay.Service.Interface;

namespace YourWay.Service.Implementation
{
    public class AccountService : IAccountService
    {
        private IAccountRepository accountRepository;
        public AccountService(IAccountRepository accountRepository){
            this.accountRepository = accountRepository;
        }


        public bool Create(Account entity)
        {
            return accountRepository.Create(entity);
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Account> FindAll()
        {
            return accountRepository.FindAll();
        }

        public Account FindById(int id)
        {
            return accountRepository.FindById(id);
        }

        public bool Update(Account entity)
        {
            throw new System.NotImplementedException();
        }
    }
}