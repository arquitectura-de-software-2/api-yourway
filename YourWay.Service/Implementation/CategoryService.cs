using System.Collections.Generic;
using YourWay.Entity;
using YourWay.Repository.Interface;
using YourWay.Service.Interface;

namespace YourWay.Service.Implementation
{
    public class CategoryService : ICategoryService
    {
        private ICategoryRepository categoryRepository;
        public CategoryService(ICategoryRepository categoryRepository){
            this.categoryRepository = categoryRepository;
        }


        public bool Create(Category entity)
        {
            return categoryRepository.Create(entity);
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Category> FindAll()
        {
            return categoryRepository.FindAll();
        }

        public Category FindById(int id)
        {
            return categoryRepository.FindById(id);
        }

        public bool Update(Category entity)
        {
            throw new System.NotImplementedException();
        }
    }
}