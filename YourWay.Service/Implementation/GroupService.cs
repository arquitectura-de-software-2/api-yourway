using System.Collections.Generic;
using YourWay.Entity;
using YourWay.Repository.Interface;
using YourWay.Service.Interface;

namespace YourWay.Service.Implementation
{
    public class GroupService : IGroupService
    {
        private IGroupRepository groupRepository;
        public GroupService(IGroupRepository groupRepository){
            this.groupRepository = groupRepository;
        }


        public bool Create(Group entity)
        {
            return groupRepository.Create(entity);
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Group> FindAll()
        {
            return groupRepository.FindAll();
        }

        public Group FindById(int id)
        {
            return groupRepository.FindById(id);
        }

        public bool Update(Group entity)
        {
            throw new System.NotImplementedException();
        }
    }
}