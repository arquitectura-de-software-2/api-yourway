using Microsoft.AspNetCore.Mvc;
using YourWay.Entity;
using YourWay.Service.Interface;

namespace YourWay.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SportController:ControllerBase
    {
        private ISportService sportService;
        public SportController(ISportService sportService){
            this.sportService = sportService;
        }


        [HttpGet]
        public ActionResult Get(){
            return Ok(sportService.FindAll());
        }
        [HttpPost]
        public ActionResult Post([FromBody] Sport sport){
            return Ok(sportService.Create(sport));
        }
        [HttpPut]
        public ActionResult Put([FromBody] Sport sport){
            return Ok(sportService.Update(sport));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(sportService.FindById(id));
        }
    }
}