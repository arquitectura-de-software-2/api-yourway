using Microsoft.AspNetCore.Mvc;
using YourWay.Entity;
using YourWay.Service.Interface;

namespace YourWay.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController: ControllerBase
    {
        private IAccountService accountService;
        public AccountController(IAccountService accountService){
            this.accountService = accountService;
        }


        [HttpGet]
        public ActionResult Get(){
            return Ok(accountService.FindAll());
        }
        [HttpPost]
        public ActionResult Post([FromBody] Account account){
            return Ok(accountService.Create(account));
        }
        [HttpPut]
        public ActionResult Put([FromBody] Account account){
            return Ok(accountService.Update(account));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(accountService.FindById(id));
        }
    }
}