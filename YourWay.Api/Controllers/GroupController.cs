using Microsoft.AspNetCore.Mvc;
using YourWay.Entity;
using YourWay.Service.Interface;

namespace YourWay.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GroupController:ControllerBase
    {
        private IGroupService groupService;
        public GroupController(IGroupService groupService){
            this.groupService = groupService;
        }


        [HttpGet]
        public ActionResult Get(){
            return Ok(groupService.FindAll());
        }
        [HttpPost]
        public ActionResult Post([FromBody] Group group){
            return Ok(groupService.Create(group));
        }
        [HttpPut]
        public ActionResult Put([FromBody] Group group){
            return Ok(groupService.Update(group));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(groupService.FindById(id));
        }
    }
}