using Microsoft.AspNetCore.Mvc;
using YourWay.Entity;
using YourWay.Service.Interface;

namespace YourWay.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShopController:ControllerBase
    {
        private IShopService shopService;
        public ShopController(IShopService shopService){
            this.shopService = shopService;
        }


        [HttpGet]
        public ActionResult Get(){
            return Ok(shopService.FindAll());
        }
        [HttpPost]
        public ActionResult Post([FromBody] Shop shop){
            return Ok(shopService.Create(shop));
        }
        [HttpPut]
        public ActionResult Put([FromBody] Shop shop){
            return Ok(shopService.Update(shop));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(shopService.FindById(id));
        }
    }
}