using Microsoft.AspNetCore.Mvc;
using YourWay.Entity;
using YourWay.Service.Interface;

namespace YourWay.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QualificationController:ControllerBase
    {
        private IQualificationService qualificationService;
        public QualificationController(IQualificationService qualificationService){
            this.qualificationService = qualificationService;
        }


        [HttpGet]
        public ActionResult Get(){
            return Ok(qualificationService.FindAll());
        }
        [HttpPost]
        public ActionResult Post([FromBody] Qualification qualification){
            return Ok(qualificationService.Create(qualification));
        }
        [HttpPut]
        public ActionResult Put([FromBody] Qualification qualification){
            return Ok(qualificationService.Update(qualification));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(qualificationService.FindById(id));
        }
    }
}