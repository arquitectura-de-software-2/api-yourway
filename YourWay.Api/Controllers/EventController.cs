using Microsoft.AspNetCore.Mvc;
using YourWay.Entity;
using YourWay.Service.Interface;

namespace YourWay.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventController:ControllerBase
    {
        private IEventService eventService;
        public EventController(IEventService eventService){
            this.eventService = eventService;
        }


        [HttpGet]
        public ActionResult Get(){
            return Ok(eventService.FindAll());
        }
        [HttpPost]
        public ActionResult Post([FromBody] Event Event){
            return Ok(eventService.Create(Event));
        }
        [HttpPut]
        public ActionResult Put([FromBody] Event Event){
            return Ok(eventService.Update(Event));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(eventService.FindById(id));
        }
    }
}