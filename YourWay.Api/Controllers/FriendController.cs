using Microsoft.AspNetCore.Mvc;
using YourWay.Entity;
using YourWay.Service.Interface;

namespace YourWay.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FriendController:ControllerBase
    {
        private IFriendService friendService;
        public FriendController(IFriendService friendService){
            this.friendService = friendService;
        }


        [HttpPost]
        public ActionResult Post([FromBody] Friend friend){
            return Ok(friendService.Create(friend));
        }
    }
}