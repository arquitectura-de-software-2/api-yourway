using Microsoft.AspNetCore.Mvc;
using YourWay.Entity;
using YourWay.Service.Interface;

namespace YourWay.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyController:ControllerBase
    {
        private ICompanyService companyService;
        public CompanyController(ICompanyService companyService){
            this.companyService = companyService;
        }
        

        [HttpGet]
        public ActionResult Get(){
            return Ok(companyService.FindAll());
        }
        [HttpPost]
        public ActionResult Post([FromBody] Company company){
            return Ok(companyService.Create(company));
        }
        [HttpPut]
        public ActionResult Put([FromBody] Company company){
            return Ok(companyService.Update(company));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(companyService.FindById(id));
        }
    }
}