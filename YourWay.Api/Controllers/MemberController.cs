using Microsoft.AspNetCore.Mvc;
using YourWay.Entity;
using YourWay.Service.Interface;

namespace YourWay.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MemberController:ControllerBase
    {
        private IMemberService memberService;
        public MemberController(IMemberService memberService){
            this.memberService = memberService;
        }


        [HttpPost]
        public ActionResult Post([FromBody] Member member){
            return Ok(memberService.Create(member));
        }
    }
}