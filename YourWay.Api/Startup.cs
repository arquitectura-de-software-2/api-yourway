﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using YourWay.Repository.Context;
using YourWay.Repository.Interface;
using YourWay.Repository.Implementation;
using YourWay.Service.Interface;
using YourWay.Service.Implementation;
using Swashbuckle.AspNetCore.Swagger;

namespace YourWay.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options=>options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddTransient<IAccountRepository, AccountRepository>();
            services.AddTransient<IAccountService, AccountService>();
            services.AddTransient<ICategoryRepository, CategoryRepository>();
            services.AddTransient<ICategoryService, CategoryService>();
            services.AddTransient<ICompanyRepository, CompanyRepository>();
            services.AddTransient<ICompanyService, CompanyService>();
            services.AddTransient<ICompetitorRepository, CompetitorRepository>();
            services.AddTransient<ICompetitorService, CompetitorService>();
            services.AddTransient<IEventRepository, EventRepository>();
            services.AddTransient<IEventService, EventService>();
            services.AddTransient<IEventSportRepository, EventSportRepository>();
            services.AddTransient<IEventSportService, EventSportService>();
            services.AddTransient<IFriendRepository, FriendRepository>();
            services.AddTransient<IFriendService, FriendService>();
            services.AddTransient<IGroupRepository, GroupRepository>();
            services.AddTransient<IGroupService, GroupService>();
            services.AddTransient<ILocationRepository, LocationRepository>();
            services.AddTransient<ILocationService, LocationService>();
            services.AddTransient<IMemberRepository, MemberRepository>();
            services.AddTransient<IMemberService, MemberService>();
            services.AddTransient<IOrderRepository, OrderRepository>();
            services.AddTransient<IOrderService, OrderService>();
            services.AddTransient<IOrderProductRepository, OrderProductRepository>();
            services.AddTransient<IOrderProductService, OrderProductService>();
            services.AddTransient<IPracticeRepository, PracticeRepository>();
            services.AddTransient<IPracticeService, PracticeService>();
            services.AddTransient<IProductRepository, ProductRepository>();
            services.AddTransient<IProductService, ProductService>();
            services.AddTransient<IQualificationRepository, QualificationRepository>();
            services.AddTransient<IQualificationService, QualificationService>();
            services.AddTransient<IShopRepository, ShopRepository>();
            services.AddTransient<IShopService, ShopService>();
            services.AddTransient<ISportRepository, SportRepository>();
            services.AddTransient<ISportService, SportService>();
            services.AddTransient<ISportsmanRepository, SportsmanRepository>();
            services.AddTransient<ISportsmanService, SportsmanService>();


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            // https://localhost:5001/swagger/index.html
            services.AddSwaggerGen (swagger => {
                var contact = new Contact () { Name = SwaggerConfiguration.ContactName, Url = SwaggerConfiguration.ContactUrl };
                swagger.SwaggerDoc (SwaggerConfiguration.DocNameV1,
                    new Info {
                        Title = SwaggerConfiguration.DocInfoTitle,
                            Version = SwaggerConfiguration.DocInfoVersion,
                            Description = SwaggerConfiguration.DocInfoDescription,
                            Contact = contact
                    }
                );
            });

            services.AddCors (options => {
                options.AddPolicy ("Todos",
                    builder => builder.WithOrigins ("*").WithHeaders ("*").WithMethods ("*"));
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseSwagger();
            app.UseSwaggerUI (c => {
                c.SwaggerEndpoint (SwaggerConfiguration.EndpointUrl, SwaggerConfiguration.EndpointDescription);
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseCors("Todos");
            // app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
